import sys
from sys import exc_info
import base64
import hashlib
import requests.utils
import requests
from faker import Faker
import random
import string

ERROR = 110
SUCCESS = 101
CORRUPT = 102
MUMBLE = 103
DOWN = 104

from utils import (
    rand_str,
    string2numeric_hash,
    traceback_to_string,
    RestException,
)

from methods.result import (
    Result,
    SUCCESS,
    CORRUPT,
    MUMBLE,
    ERROR,
    DOWN,
)


def close(code, public="", private=""):
    if public:
        print(public)
    if private:
        print(private, file=sys.stderr)
    print('Exit with code {}'.format(code), file=sys.stderr)
    exit(code)

fake = Faker()


def put_flag(host, flag, vuln):
    vuln = int(vuln)
    if not (1 <= vuln <= 4):
        print("Invalid vuln", file=sys.stderr)
        return ERROR
    try:
        with requests.Session() as s:
            if vuln == 1:
                return put_first_vuln_flag(s, host, flag)
            else:
                print("Invalid vuln", file=sys.stderr)
                return close(ERROR, private="Wrong command")
    except RestException as re:
        print(re, file=sys.stderr)
        return Result(MUMBLE, 'Service API Error', str(re))    
#        print(re, file=sys.stderr)
#        return close(MUMBLE, "Server does not return 200")
    except requests.exceptions.ConnectionError as ce:
        print(ce, file=sys.stderr)
        return close(DOWN, "Server unreachable" )
    except:
        print('unknown err', exc_info())
        return close(MUMBLE, "Server does not return 200")

def put_first_vuln_flag(uid, host, flag):
    s = requests.Session()
 #   try:
 #   s = requests.Session()
    login = flag[:5]
    password = hashlib.md5(flag.encode('utf-8')).hexdigest()
    print(login,password,host)
    tim = random.randint(1, 50)
    s.post("http://{}:10000/register".format(host), data={"username" : login, "password" : password, "description": flag})
    s.post("http://{}:10000/".format(host), data={"username" : login, "password" : password})
    s.post("http://{}:10000/add_monitoring".format(host), data={"hostname" : login+'.fly', "type": 'HTTP', 'timeout' : tim, 'agent': flag})
    #close(SUCCESS, 'flag is still here')
    return Result(SUCCESS, 'flag is still here')
 #   except Exception as e:
 #       close(CORRUPT, 'Flag not found') 





