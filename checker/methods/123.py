import sys
from sys import exc_info

import requests.utils
from faker import Faker
ERROR = 110
SUCCESS = 101
CORRUPT = 102
MUMBLE = 103
DOWN = 104

fake = Faker()


def health_check(host):
    # check some service endpoints to make sure it works correctly
    s = requests.Session()
    try:
        r = s.get("http://{}:10000/".format(host))
        print(r.status_code)
        if 200 == r.status_code:
            print(SUCCESS, "It's alive!")
        else:
            print(MUMBLE, 'Server does not return 200')
    except Exception as e:
        print(DOWN, 'Service is down')
 
health_check('192.168.31.251')       
