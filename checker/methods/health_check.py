import sys
from sys import exc_info
import base64
import hashlib
import requests
import random
import string

import requests.utils
from faker import Faker

ERROR = 110
SUCCESS = 101
CORRUPT = 102
MUMBLE = 103
DOWN = 104

from methods.result import (
    Result,
    SUCCESS,
    MUMBLE,
    DOWN,
    ERROR,
)
from s4femap.requests import (
    registration,
    login,
    create_mark,
    like_mark,
    comment_mark,
    get_liked_marks,
    get_couriers,
    get_bunkers,
)
from utils import (
    rand_str,
    string2numeric_hash,
    traceback_to_string,
    RestException,
    InconsistentException,
)
from settings import (
    DEBUG,
)

fake = Faker()


def health_check(host):
    # check some service endpoints to make sure it works correctly
    s = requests.Session()
  #  try:
    r = s.get("http://{}:10000/".format(host))
    print(r.status_code)
    if 200 == r.status_code:
        return Result(SUCCESS, "It's alive!")
    else:
        return Result(MUMBLE, 'Server does not return 200')
    #except Exception as e:
     #   Result(DOWN, 'Service is down')
        