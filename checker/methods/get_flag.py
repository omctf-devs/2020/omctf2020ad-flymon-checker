import sys
from sys import exc_info
import base64
import hashlib
import requests.utils
import requests
import random
import string

from methods.result import (
    Result,
    SUCCESS,
    CORRUPT,
    MUMBLE,
    ERROR,
    DOWN,
)
from s4femap.requests import (
    login,
    get_liked_marks,
    get_mark_comments,
)
from utils import (
    traceback_to_string,
    RestException,
    InconsistentException,
)


def get_flag(host, uid, flag, vuln):
    # check that flag exists in certain vulnerability
    try:
        with requests.Session() as s:
            if vuln == 1:
                return get_first_vuln_flag(s, host, uid, flag)
            else:
                print("Invalid vuln", file=sys.stderr)
                return Result(ERROR, ('Invalid vulnerability index: %s' % vuln))
    except RestException as re:
        print(re, file=sys.stderr)
        return Result(MUMBLE, 'Service API Error', str(re))
    except requests.exceptions.ConnectionError as ce:
        print(ce, file=sys.stderr)
        return Result(DOWN, 'Service is down', str(ce))
    except:
        print('unknown err', exc_info())
        return Result(ERROR, 'Unknown error', traceback_to_string())
   

def get_first_vuln_flag(session, host, uid, flag):
    s = requests.Session()
#    try:
    login = flag[:5]
    password =  hashlib.md5(flag.encode('utf-8')).hexdigest()
    s.post("http://{}:10000".format(host), data={"username" : login, "password" : password})
    req = s.get("http://{}:10000/profile".format(host)).text
    if flag in req:
        return Result(SUCCESS, 'flag is still here')
    else:    
        return Result(CORRUPT, 'flag not found')
        #req1 = s.get("http://{}:{}/tables".format(host, PORT)).text
 #   except Exception as e:
 #       return Result(CORRUPT, 'flag is corrupted')


