BASE_URL = 'http://%s/api'

url_map = {
    'user': BASE_URL + '/user',
    'login': BASE_URL + '/user/session',
    'mark': BASE_URL + '/mark',
    'mark_type': BASE_URL + '/mark-type',
    'bunkers': BASE_URL + '/bunkers',
}


def get_url(host, endpoint):
    return url_map[endpoint] % host
